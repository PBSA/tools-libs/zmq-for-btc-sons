# ZMQ utility for BTC-SONs

This ZMQ utility can be used to subscribe to ZMQ messages being published by a Bitcoin node. This can work with local Bitcoin nodes (`127.0.0.1`) or external nodes with open ZMQ ports.

## Environment Variables

A `.env` file must exist in the apps' root directory. A `.env.example` file has been provided as a reference for the `.env` file you must create.

## Installation

1. Clone the repo.

```shell
cd $HOME
git clone https://gitlab.com/PBSA/tools-libs/zmq-for-btc-sons
cd zmq-for-btc-sons
```

2. Install the dependencies.

```shell
npm install
```

3. Configure the `.env` file.

```shell
cp ./.env.example .env
sudo vim ./.env
# you can use vi, vim, nano...
# Now you'll edit and save the .env file based on your needs.
```

4. Run the ZMQ utility.

```shell
npm start
```

If you need to exit the utility, press `Ctrl + C`. You can always edit the `.env` file to reconfigure the utility.
