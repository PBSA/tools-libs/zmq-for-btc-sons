FROM registry.gitlab.com/pbsa/peerplays-environment/ubuntu-for-peerplays:develop
MAINTAINER PeerPlays Blockchain Standards Association

 
RUN apt-get update -y \
    && apt-get install -y curl gnupg \
    && curl -sL https://deb.nodesource.com/setup_16.x | bash \
    && apt-get install nodejs -y

WORKDIR /app
COPY . /app/
RUN npm install

COPY ./.env.example .env

ENTRYPOINT ["npm", "start"]
