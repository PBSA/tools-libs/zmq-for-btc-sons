const zmq = require("zeromq")
require('dotenv').config()
const RpcClient = require('bitcoind-rpc')

// ZMQ .env settings
const PUB_HOST = process.env.PUB_HOST
const PUB_PORT = process.env.PUB_PORT
const TOPIC    = process.env.TOPIC

// RPC .env settings
const usingRpc = process.env.USE_RPC
const RPC_PROTOCOL = process.env.BTC_NODE_RPC_PROTOCOL
const RPC_USER = process.env.BTC_NODE_RPC_USER
const RPC_PASS = process.env.BTC_NODE_RPC_PASS
const RPC_HOST = process.env.BTC_NODE_RPC_HOST
const RPC_PORT = process.env.BTC_NODE_RPC_PORT


async function run() {
  console.log('Starting ZMQ Subscriber')
  const sock = new zmq.Subscriber
  let rpc, rpcConfig, usingRawTx = false
  if (TOPIC.indexOf('raw') >= 0) { usingRawTx = true }
  console.log('config: Using RPC? %s', usingRpc)
  console.log('config: Using RawTx? %s', usingRawTx)
  if (usingRpc) {
    rpcConfig = {
      protocol: RPC_PROTOCOL,
      user: RPC_USER,
      pass: RPC_PASS,
      host: RPC_HOST,
      port: RPC_PORT,
    }
    rpc = new RpcClient(rpcConfig)
  }

  const connString = 'tcp://' + PUB_HOST + ':' + PUB_PORT
  sock.connect(connString)
  sock.subscribe(TOPIC)
  console.log('Subscriber connected to publisher at: %s', connString)
  console.log('Looking for messages on topic: %s', TOPIC)

  if (usingRpc && usingRawTx) {
    for await (const [topic, msg] of sock) {
      rpc.decodeRawTransaction(msg.toString('hex'), function(err, res) {
        console.log('\n--New Message--\nTopic: %s', topic)
        console.log(JSON.stringify(res, null, 2))
        if (err) {
          console.log('Error: %s', err)
        }
      })
    }
  }
  else {
    for await (const [topic, msg] of sock) {
      console.log('\n--New Message--\nTopic: %s\nMessage: %s\n', topic, msg)
    }
  }
}

run()